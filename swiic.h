#ifndef __SWIIC_H_
#define __SWIIC_H_

#include "stm32f10x.h"  //GPIO 初始化

#define OUTTIME 250     //ACK超时时间
#define IIC_Delay() 	delay_us(3)     //设定电平保持时间，经过测量，在主频72M时3us IIC时钟速度为100K

#define __SDA_1 GPIO_SetBits(IIC->SDA.Port,IIC->SDA.Pin)    //SDA置高
#define __SDA_0 GPIO_ResetBits(IIC->SDA.Port,IIC->SDA.Pin)  //SDA置低

#define __SCL_1 GPIO_SetBits(IIC->SCL.Port,IIC->SCL.Pin)    //SCL置高
#define __SCL_0 GPIO_ResetBits(IIC->SCL.Port,IIC->SCL.Pin)  //SCL置低

#define	__SDA_READ GPIO_ReadInputDataBit(IIC->SDA.Port,IIC->SDA.Pin)    //读取SDA

#define SDA_IN()  {IIC->SDA.Port->CRL &= 0X0FFFFFFF; IIC->SDA.Port->CRL |= (u32)8<<28;} //更改SDA模式为输入
#define SDA_OUT() {IIC->SDA.Port->CRL &= 0X0FFFFFFF; IIC->SDA.Port->CRL |= (u32)3<<28;} //更改SDA模式为输出

#define 	IIC_MEMADD_SIZE_8BIT    			8       //寄存器地址为8位地址  
#define		IIC_MEMADD_SIZE_16BIT   			16      //寄存器地址为16位地址
extern enum __IIC_MEMADD_SIZE IIC_MEMADD_SIZE;          

typedef struct{
	uint32_t AHB;
	GPIO_TypeDef *Port;
	uint16_t Pin;
}IIC_GPIO;

typedef struct{
	IIC_GPIO SCL;
	IIC_GPIO SDA;
	uint8_t status;
	uint8_t addr;
}IIC_InitTypeDef;

extern IIC_GPIO RISER_SCL;
extern IIC_GPIO RISER_SDA;
extern IIC_InitTypeDef RISER_IIC;

void IIC_Start(IIC_InitTypeDef* IIC);
void IIC_Stop(IIC_InitTypeDef* IIC);
uint8_t IIC_WaitAck(IIC_InitTypeDef* IIC);
void IIC_Ack(IIC_InitTypeDef* IIC);
void IIC_NAck(IIC_InitTypeDef* IIC);
void IIC_SendByte(IIC_InitTypeDef* IIC,uint8_t _ucByte);
uint8_t IIC_ReadByte(IIC_InitTypeDef* IIC,uint8_t *read);

void IIC_Init(IIC_InitTypeDef* IIC);

uint8_t IIC_Read_Data(IIC_InitTypeDef* IIC,uint8_t *pData,u16 Len);
uint8_t IIC_Read(IIC_InitTypeDef* IIC,uint8_t *pData);
uint8_t IIC_Write(IIC_InitTypeDef* IIC,uint8_t *pData);
uint8_t IIC_Mem_Read(IIC_InitTypeDef* IIC,u16 MemAddr,uint8_t MemAddSize,u16 Size,uint8_t *pData);
uint8_t IIC_Mem_Write(IIC_InitTypeDef* IIC,u16 MemAddr,uint8_t MemAddSize,u16 Size,uint8_t *pData);

//示例复用IIC-Pin
#define    	IIC_SDA_AHB    RCC_APB2Periph_GPIOB
#define    	IIC_SDA_Port   GPIOB
#define    	IIC_SDA_Pin    GPIO_Pin_7
	
#define    	IIC_SCL_AHB    RCC_APB2Periph_GPIOB
#define    	IIC_SCL_Port   GPIOB
#define    	IIC_SCL_Pin    GPIO_Pin_6
void IIC_ONE_Init(void);
//示例复用IIC-Pin

#endif //__SWIIC_H_