用于STM32-M3上的软件IIC驱动，

使用结构体封装函数，通过新建不同的结构体来做到开启多路IIC通道

提升代码复用率，减少垃圾代码。

### 使用demo


/*使用独立IIC引脚*/


```
#include "swiic.h"

#define    	IIC2_SDA_AHB    RCC_APB2Periph_GPIOB
#define    	IIC2_SDA_Port   GPIOB
#define    	IIC2_SDA_Pin    GPIO_Pin_7
	
#define    	IIC2_SCL_AHB    RCC_APB2Periph_GPIOB
#define    	IIC2_SCL_Port   GPIOB
#define    	IIC2_SCL_Pin    GPIO_Pin_6

IIC_InitTypeDef LM75B;

u8 LM75BInit(void)

{
	
	LM75B.SDA.AHB = IIC2_SDA_AHB;

	LM75B.SDA.Port= IIC2_SDA_Port;

	LM75B.SDA.Pin = IIC2_SDA_Pin;
												 
	LM75B.SCL.AHB = IIC2_SCL_AHB;

	LM75B.SCL.Port= IIC2_SCL_Port;

	LM75B.SCL.Pin = IIC2_SCL_Pin;

	LM75B.addr = 0xA0;
	
	IIC_Init(&LM75B);

}
```






/*或者使用同一组IIC引脚*/


```
#include "swiic.h"

#define    	IIC2_SDA_AHB    RCC_APB2Periph_GPIOB

#define    	IIC2_SDA_Port   GPIOB

#define    	IIC2_SDA_Pin    GPIO_Pin_7
	
#define    	IIC2_SCL_AHB    RCC_APB2Periph_GPIOB

#define    	IIC2_SCL_Port   GPIOB

#define    	IIC2_SCL_Pin    GPIO_Pin_6

IIC_GPIO IIC_SCL={IIC2_SCL_AHB,IIC2_SCL_Port,IIC2_SCL_Pin};

IIC_GPIO IIC_SDA={IIC2_SDA_AHB,IIC2_SDA_Port,IIC2_SDA_Pin};

void IIC_PIN_Init(void){

	IIC_InitTypeDef IIC_PIN;

	IIC_PIN.SCL = IIC_SCL;

	IIC_PIN.SDA = IIC_SDA;

	IIC_Init(&IIC_PIN);
}

IIC_InitTypeDef LM75B;

u8 LM75BInit(void)

{
	LM75B.SDA = IIC_SDA	
				 
	LM75B.SCL = IIC_SCL;

	LM75B.addr = 0xA0;
}
```
